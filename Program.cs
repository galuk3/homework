﻿using System;

namespace homework
{

    class Program
    {
        string[] colors = { "green", "red", "yellow" };
        static string[] balls = { "green", "red", "yellow", "green","red","yellow" };

        static void printArray(ref string[] balls)
        {
            for (int i = 0; i < balls.Length; i++)
            {
                Console.WriteLine(balls[i]);
            }
        }
        static void sort(ref string[] balls)
        {
             int countGreen = 0;
             int countRed = 0;
             int countYellow = 0;

            for (int j=0;j<balls.Length;j++)
            {
                switch (balls[j])
                {
                    case "green":
                        countGreen++;
                       // Console.WriteLine(countGreen);
                        break;
                    case "yellow":
                        countYellow++;
                        break;
                    case "red":
                        countRed++;
//                        Console.WriteLine(countRed);

                        break;
                }
                

            }
            int i;
            for (i = 0; i < countGreen; i++)
            {
                balls[i] = "green";
            }
            for (i = countGreen; i < countYellow + countGreen; i++)
            {
                balls[i] = "yellow";
            }
            for (i = countGreen + countYellow; i < countGreen + countYellow + countRed; i++)
            {
                balls[i] = "red";
            }
        }
        static void Main(string[] args)
        {
            sort(ref balls);
            printArray(ref balls);
        }
    }
}
