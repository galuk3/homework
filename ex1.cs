﻿using System;

namespace ex1
{

    class ex1
    {
        //kind of bucket sort O(n)
        string[] colors = { "green", "red", "yellow" };
        static string[] balls = { "green", "red", "yellow", "green","red","yellow" };

        static void printArray(ref string[] balls)
        {
            //prints array values
            for (int i = 0; i < balls.Length; i++)
            {
                Console.WriteLine(balls[i]);
            }
        }
        static void sort(ref string[] balls)
        {
            //sorting the balls green->yellow->red
             int countGreen = 0;
            int countYellow = 0;
            int countRed = 0;

            for (int j=0;j<balls.Length;j++)
            {
                switch (balls[j])
                {
                    case "green":
                        countGreen++;
                        break;
                    case "yellow":
                        countYellow++;
                        break;
                    case "red":
                        countRed++;
                        break;
                }
                

            }
            int i;
            for (i = 0; i < countGreen; i++)
            {
                balls[i] = "green";
            }
            for (i = countGreen; i < countYellow + countGreen; i++)
            {
                balls[i] = "yellow";
            }
            for (i = countGreen + countYellow; i < countGreen + countYellow + countRed; i++)
            {
                balls[i] = "red";
            }
        }
        public static void Main1()
        {
            sort(ref balls);
            printArray(ref balls);
        }
    }
}
