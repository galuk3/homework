﻿using System;
using System.Collections.Generic;

namespace ex1
{
    class ex3:ex2
    {
        //made a stack algorithm O(n)
        string exp2 = "{()[]}}";
        public static Stack<char> WaitingToBeClosed = new Stack<char>();
        public static char GetOpener(char closer)
        {
            switch (closer)
            {
                case '}':
                    return '{';
                case ')':
                    return '(';
                case ']':
                    return '[';
                default: return '0';
            }
        }
        public static bool IsValid(string exp)
        {
            for (int j = 0; j < exp.Length; j++)
            {
                switch (exp[j])
                {
                    case '{':
                    case '[':
                    case '(':
                    
                        WaitingToBeClosed.Push(exp[j]);
                            break;
                    case ')':
                    case ']':
                    case '}':
                        if (WaitingToBeClosed.Count==0 || GetOpener(exp[j]) != WaitingToBeClosed.Pop())
                            return false;
                        break;
                }
            }
            if (WaitingToBeClosed.Count == 0)
                return true;
            else
                return false;
        }
        public static void Main3()
        {
            string exp = "{ga[lb]laba}fg[f]";

            Console.WriteLine(IsValid(exp));
        }
    }
}
