﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ex1
{
    class ex2:ex1
    {
        static List<T> randomize<T>(ref List<T> src)
        {
            // random list algorithm O(n)
            var rand = new Random();
            List<T> res = new List<T>();
            int sLen = src.Count;
            for (int i = 0; i < sLen; i++)
            {
                int rndNum = rand.Next(0, src.Count);
                res.Add(src.ElementAt(rndNum));
                src.RemoveAt(rndNum);
            }

            return res;
        }

        public static void Main2()
        {
            List<string> songs = new List<string>();
            songs.Add("first");
            songs.Add("second");
            songs.Add("third");
            songs.Add("fourth");


            var res = randomize(ref songs);
            foreach (var val in res)
            {
                Console.WriteLine(val);
            }
        }
    }
}