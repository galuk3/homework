﻿using System;
using System.Collections.Generic;
namespace ex1
{
    class MyDS
    {
        //all methods are in O(1)
        private int defaultValue;
        private Dictionary<int, int> dict;

        public MyDS()
        {
            defaultValue = 0;  //initialize default value
            dict = new Dictionary<int, int>();
        }

        public void setAll(int val)
        {
            defaultValue = val;
            dict = new Dictionary<int, int>();
        }

        public void setValue(int index, int val)
        {
            if (dict.ContainsKey(index))
                dict[index] = val;
            else
                dict.Add(index, val);
        }

        public int getValue(int index)
        {
            if (dict.ContainsKey(index))
                return dict[index];
            else
                return defaultValue;
        }
    }
    class ex4:ex3
    {
        public static void Main4()
        {
            MyDS dict=new MyDS();
            dict.setValue(0, 1);
            dict.setValue(0, 2);
            Console.WriteLine(dict.getValue(0));
            dict.setAll(5);
            Console.WriteLine(dict.getValue(0));
            Console.WriteLine(dict.getValue(1));
            dict.setValue(0, 2);
            Console.WriteLine(dict.getValue(0));
        }
    }
}
